delete from blc_locale where locale_code = 'th_TH';
delete from blc_currency where currency_code = 'THB';
INSERT INTO blc_currency (currency_code, default_flag, friendly_name) VALUES ('THB', true, 'THB Baht');

INSERT INTO blc_locale (locale_code, default_flag, friendly_name, use_in_search_index, currency_code) VALUES ('th_TH', null,  'Thai', null, 'THB');


update blc_locale
set default_flag = false;

update blc_locale
set default_flag = true
where currency_code = 'THB';

update blc_currency
set default_flag = false;

update blc_currency
set default_flag = true
where currency_code = 'THB';
