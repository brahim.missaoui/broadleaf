
delete from blc_category_xref where category_xref_id = 10;
delete from blc_category where category_id = 2009;

insert into blc_category(category_id, active_end_date, active_start_date, archived, description, display_template, external_id, fulfillment_type, inventory_type, long_description, meta_desc, meta_title, name, override_generated_url, product_desc_pattern_override, product_title_pattern_override, root_display_order, tax_code, url, url_key, default_parent_category_id)
values (2009,null,'2019-11-06 08:31:56.000000',null,'เครื่องจักรกลเกษตร',null,null,null,null,null,null,null,'เครื่องจักรกลเกษตร',false,null,null,null,null,'/agricultural-machinery',null,null);


update blc_cms_menu_item
set action_url = '/', label='หน้าแรก'
where menu_item_id =1;


update blc_cms_menu_item
set action_url = '/new-items', label='สินค้าใหม่'
where menu_item_id =2;

update blc_cms_menu_item
set action_url = '/product-promotion', label='สินค้าโปรโมชัน'
where menu_item_id =3;


update blc_cms_menu_item
set action_url = '/category', label='หมวดหมู่'
where menu_item_id =4;


update  blc_category
set description = 'หน้าแรก', name = 'หน้าแรก', url='/'
where category_id = 2001;



update  blc_category
set description = 'สินค้าใหม่', name = 'สินค้าใหม่', url='/new-items'
where category_id = 2002;

update  blc_category
set description = 'สินค้าโปรโมชัน', name = 'สินค้าโปรโมชัน', url='/product-promotion'
where category_id = 2003;

update  blc_category
set description = 'หมวดหมู่', name = 'หมวดหมู่', url='/category'
where category_id = 2004;

update  blc_category
set description = 'ปุ๋ย', name = 'ปุ๋ย', url='/fertilizer'
where category_id = 2007;

update  blc_category
set description = 'เครื่องมือการเกษตร', name = 'เครื่องมือการเกษตร', url='/agricultura-tools'
where category_id = 2008;


update blc_category_xref set category_id = 2004
where category_xref_id = 8;

update blc_category_xref set category_id = 2004
where category_xref_id = 9;

insert into  blc_category_xref(category_xref_id, default_reference, display_order, category_id, sub_category_id)
values (10,true,-5.000000,2004,2009);



delete from blc_cat_search_facet_xref
where category_search_facet_id =4;

delete from blc_cat_search_facet_xref
where category_search_facet_id =6;

delete from blc_cat_site_map_gen_cfg
where root_category_id =2003;

delete from blc_category_product_xref
where category_id = 2003;

delete from blc_product_featured
where category_id = 2003;

delete  from blc_category
where category_id = 2003;