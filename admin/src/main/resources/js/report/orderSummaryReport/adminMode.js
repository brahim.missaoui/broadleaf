let isAdmin = false;
let isFacAdmin = false;
let defaultFacCode = ''
$(document).ready(function () {
    console.log('report order summary listView')
    checkIsAdmin()
    setComponentScreen()
    $('#btn_export').on('click',function () {

        console.log('btn_export order summary !')
        let facCodeCompare = isFacAdmin === true ? defaultFacCode:$('#search_by_fac').val()
        let param = '' ;
            param += '?manufac='+$('#search_by_manufac').val() ;
            param += '&facCode='+facCodeCompare
            param += '&year='+$('#search_by_year option:selected').val() ;
        console.log('param = '+param)
        window.location.href = window.location.origin+$('#contextRoot').val()+'report/orderSummaryReport/generateReportAdmin'+param
    })

    renderListYearSearch()
})

function checkIsAdmin(){
    let sizeRole = $('.sub-role-style').length
    for(let v=0; v<sizeRole; v++){
        if ($('.sub-role-style')[v].innerHTML == 'ROLE_ADMIN' ){
            isAdmin = true;
        }else if($('.sub-role-style')[v].innerHTML == 'ROLE_FACTORY_ADMIN'){
            isFacAdmin = true;
            defaultFacCode = ($('#facCode_User').val().split('FACTORY_CODE=')[1].split(',')[0]).replace('}','')
        }
    }

}
function setComponentScreen() {
    if(isAdmin == true){
        $('#div_search_fac').removeClass('hide-block')
    }else{
        $('#div_search_fac').addClass('hide-block')
    }

}

function renderListYearSearch() {

    let d = new Date();
    let currentYear = d.getFullYear();
    for(let i=-10; i<10; i++){
        if(parseInt(currentYear+i) == parseInt(currentYear)){
            $('#search_by_year').append('' +
                '<option selected value="'+(currentYear+i)+'">'+(currentYear+i)+'</option>' +
                '')
        }else{
            $('#search_by_year').append('' +
                '<option value="'+(currentYear+i)+'">'+(currentYear+i)+'</option>' +
                '')
        }

    }

    $('#btn_export').attr('disabled',false)
    $('#btn_export').removeClass('not-allow-button')

}
