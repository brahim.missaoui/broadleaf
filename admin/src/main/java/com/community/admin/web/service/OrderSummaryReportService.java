package com.community.admin.web.service;

import java.util.List;
import java.util.Map;

public interface OrderSummaryReportService {

	/*query for role admin, factory Admin*/
	List<Map<String,Object>> queryReportDataByYear_RoleAdmin(String manufacture, String facCode, String year);
	List<Map<String,Object>> queryReportDataByMonth_RoleAdmin(String manufacture, String facCode, String year);


	/*query for role vendor */
	List<Map<String,Object>> queryReportDataByYear_RoleVender(String manufacture, String year);
	List<Map<String,Object>> queryReportDataByMonth_RoleVende(String manufacture, String year);


}
