package com.community.admin.web.service.impl;

import com.community.admin.web.dao.OrderCustomDao;
import com.community.admin.web.model.ResponseModel;
import com.community.admin.web.repository.OrderRepositoryCustom;
import com.community.admin.web.service.OrderCustomService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.broadleafcommerce.core.order.domain.Order;
import org.springframework.jmx.export.annotation.ManagedResource;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service("blOrderCustomService")
@ManagedResource(objectName="org.broadleafcommerce:name=OrderCustomService", description="Order Custom Service", currencyTimeLimit=15)
public class OrderCustomServiceImpl implements OrderCustomService {

	private static final Log LOG = LogFactory.getLog(OrderCustomServiceImpl.class);

	@Resource(name = "blOrderCustomDao")
    protected OrderCustomDao orderCustomDao;

	@Resource(name = "blOrderCustomRepo")
	protected OrderRepositoryCustom orderRepositoryCustom;

	@Override
	public List<Order> findOrderbyCriteriaRoleFactoryAdmin(
			String facCode,
			String orderNum,
			String customer,
			String status,
			String amountSt,
			String amountEd,
			String dateSt,
			String dateEd
	) {

		return orderCustomDao.findOrderbyCriteriaRoleFactoryAdmin(facCode,orderNum,customer,status,amountSt,amountEd,dateSt,dateEd);

	}

	@Override
	public List<Order> findOrderbyCriteriaRoleAdmin(String facCode, String orderNum, String customer, String status, String amountSt, String amountEd, String dateSt, String dateEd) {
		return orderCustomDao.findOrderbyCriteriaRoleAdmin(facCode,orderNum,customer,status,amountSt,amountEd,dateSt,dateEd);
	}

	@Override
	public ResponseModel changeStatusOrder(String orderNumber, String status) {
		return orderRepositoryCustom.changeStatusOrder(orderNumber,status);
	}

}
