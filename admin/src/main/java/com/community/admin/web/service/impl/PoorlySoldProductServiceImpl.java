package com.community.admin.web.service.impl;

import com.community.admin.web.repository.PoorlyReportRepositoryCustom;
import com.community.admin.web.service.PoorlySoldProductReportService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Service("blPoorlySoldProductReportService")
public class PoorlySoldProductServiceImpl implements PoorlySoldProductReportService {

	@Resource(name = "blPoorlySoldProductReportCustomRepo")
	protected PoorlyReportRepositoryCustom poorlyReportRepositoryCustom;

	@Override
	public List<Map<String, Object>> queryReportByMonth_RoleAdmin(String manFac,String facCode, String year, String month, String limitMax) {
		return poorlyReportRepositoryCustom.queryReportByMonth_RoleAdmin(manFac,facCode, year,month,limitMax);
	}

	@Override
	public List<Map<String, Object>> queryReportByYear_RoleAdmin(String manFac,String facCode, String year, String limitMax) {
		return poorlyReportRepositoryCustom.queryReportByYear_RoleAdmin(manFac,facCode,year,limitMax);
	}

	@Override
	public List<Map<String, Object>> queryReportByMonth_RoleVender(String manFac, String year, String month, String limitMax) {
		return poorlyReportRepositoryCustom.queryReportByMonth_RoleVender(manFac,year,month,limitMax);
	}

	@Override
	public List<Map<String, Object>> queryReportByYear_RoleVender(String manFac, String year, String limitMax) {
		return poorlyReportRepositoryCustom.queryReportByYear_RoleVender(manFac,year,limitMax);
	}
}
