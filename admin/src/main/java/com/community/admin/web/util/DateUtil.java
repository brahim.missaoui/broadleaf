package com.community.admin.web.util;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DateUtil {
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd", Locale.US);


    public static Timestamp getTimeStamp(String stringDate) {
        Timestamp today = null;
        try {
            today = getDateWithRemoveTime(DATE_FORMAT.parse(stringDate));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return today;
    }

    public static Timestamp getDateWithRemoveTime(Date date) {
        Timestamp maxTimeDate = null;
        try {
            SimpleDateFormat newformat = new SimpleDateFormat("yyyy-MM-dd", getSystemLocale());
            maxTimeDate = Timestamp.valueOf(newformat.format(date) + " " + "00:00:00.000");
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return maxTimeDate;
    }

    public static Timestamp getTimeStampGetMaxTime(String stringDate) {

//        LOGGER.info()
        Timestamp today = null;
        try {
            today = getTimeMax(DATE_FORMAT.parse(stringDate));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return today;
    }

    public static Timestamp getTimeMax(Date date) {
        Timestamp maxTimeDate = null;
        try {
            SimpleDateFormat newformat = new SimpleDateFormat("yyyy-MM-dd", getSystemLocale());
            maxTimeDate = Timestamp.valueOf(newformat.format(date) + " " + "23:59:59.999");
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return maxTimeDate;
    }


    public static Locale getSystemLocale() {
        return Locale.US;
    }
}
