package com.community.admin.web.dao.impl;

import com.community.admin.web.dao.OrderCustomDao;
import com.community.admin.web.util.DateUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.broadleafcommerce.core.order.domain.Order;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Repository("blOrderCustomDao")
public class OrderCustomDaoImpl implements OrderCustomDao {
	
	private static final Log LOG = LogFactory.getLog(OrderCustomDaoImpl.class);
	
	@PersistenceContext(unitName = "blPU")
	protected EntityManager em;

	@Override
	public List<Order> findOrderbyCriteriaRoleFactoryAdmin(String facCode,
												 String orderNum,
												 String customer,
												 String status,
												 String amountSt,
												 String amountEd,
												 String dateSt,
												 String dateEd
	) {
		List<Order> resultOrder = null;
		try{
			boolean flag_check_name = false;
			boolean flag_check_status = false;
			boolean flag_check_amount_st = false;
			boolean flag_check_amount_ed = false;
			boolean flag_check_date_st = false;
			boolean flag_check_date_ed = false;

			LOG.info("facCode = "+facCode);
			LOG.info("orderNum = "+orderNum);
			LOG.info("customer = "+customer);
			LOG.info("status = "+status);
			LOG.info("amountSt = "+amountSt);
			LOG.info("amountEd = "+amountEd);
			LOG.info("dateSt = "+dateSt);
			LOG.info("dateEd = "+dateEd);

			facCode = facCode == null ? "" : facCode;
			orderNum = orderNum == null ? "" : orderNum;
			customer = customer == null ? "" : customer;
			status = status == null ? "" : status;
			amountSt = amountSt == null ? "" : amountSt;
			amountEd = amountEd == null ? "" : amountEd;
			dateSt = dateSt == null ? "" : dateSt;
			dateEd = dateEd == null ? "" : dateEd;

			StringBuilder sb = new StringBuilder();
			sb.append(" SELECT order ");
			sb.append(" FROM org.broadleafcommerce.core.order.domain.Order order ");
			sb.append(" WHERE order.customer.username like :customer ");
			sb.append(" AND order.orderNumber like :orderNum ");

			if(!"".equals(facCode) && null != facCode){
				sb.append(" AND order.name = :facCode ");
				flag_check_name=true;
			}
			if(!"ALL".equals(status) && null != status){
				sb.append(" AND order.status = :status ");
				flag_check_status=true;
			}else{
				sb.append(" AND order.status in :status_list  ");
			}
			if(!"".equals(amountSt) && null != amountSt){
				sb.append(" AND order.total >= :amountStCov ");
				flag_check_amount_st=true;
			}
			if(!"".equals(amountEd) && null != amountEd){
				sb.append(" AND order.total <= :amountEdCov ");
				flag_check_amount_ed=true;
			}
			if((!"".equals(dateSt) && null != dateSt)){
				sb.append(" AND order.submitDate >=  :stDateConv ");
				flag_check_date_st=true;
			}
			if((!"".equals(dateEd) && null != dateEd)){
				sb.append(" AND order.submitDate <=  :edDateConv ");
				flag_check_date_ed=true;
			}

			sb.append(" ORDER BY  order.submitDate ");
			LOG.info("query : "+sb.toString());
			final Query query = em.createQuery(sb.toString());

			if(flag_check_name){
				query.setParameter("facCode", facCode);
			}

			if(flag_check_status){
				query.setParameter("status", status);
//				query.setParameter("status", OrderStatus.getInstance(status));

			}else{
				List<String> listStatus = new ArrayList<>();
				listStatus.add("WAIT");
				listStatus.add("APPROVE");
				listStatus.add("REJECT");
				query.setParameter("status_list", listStatus);
			}

			if(flag_check_amount_st){
				BigDecimal amountStCov = new BigDecimal(amountSt);
				query.setParameter("amountStCov", amountStCov);
			}
			if(flag_check_amount_ed){
				BigDecimal amountEdCov = new BigDecimal(amountEd);
				query.setParameter("amountEdCov", amountEdCov);
			}
			if(flag_check_date_st){
				dateSt = dateSt.replaceAll("/","-");
				Timestamp ts = DateUtil.getTimeStamp(dateSt);
				query.setParameter("stDateConv", ts);
			}
			if(flag_check_date_ed){
				dateEd = dateSt.replaceAll("/","-");
				Timestamp ts = DateUtil.getTimeStampGetMaxTime(dateEd);
				query.setParameter("edDateConv", ts);
			}

			query.setParameter("customer", "%"+customer+"%");
			query.setParameter("orderNum", "%"+orderNum+"%");
			resultOrder = query.getResultList();

			return resultOrder;
		}catch (Exception e){
			LOG.error("findOrderbyCriteria Exception Msg : "+e.getMessage());
			e.printStackTrace();
			return	resultOrder;
		}

	}

	@Override
	public List<Order> findOrderbyCriteriaRoleAdmin(String facCode, String orderNum, String customer, String status, String amountSt, String amountEd, String dateSt, String dateEd) {

		List<Order> resultOrder = null;
		try{
			boolean flag_check_name = false;
			boolean flag_check_status = false;
			boolean flag_check_amount_st = false;
			boolean flag_check_amount_ed = false;
			boolean flag_check_date_st = false;
			boolean flag_check_date_ed = false;



			facCode = facCode == null ? "" : facCode;
			orderNum = orderNum == null ? "" : orderNum;
			customer = customer == null ? "" : customer;
			status = status == null ? "" : status;
			amountSt = amountSt == null ? "" : amountSt;
			amountEd = amountEd == null ? "" : amountEd;
			dateSt = dateSt == null ? "" : dateSt;
			dateEd = dateEd == null ? "" : dateEd;

			StringBuilder sb = new StringBuilder();
			sb.append(" SELECT order ");
			sb.append(" FROM org.broadleafcommerce.core.order.domain.Order order ");
			sb.append(" WHERE order.customer.username like :customer ");
			sb.append(" AND order.orderNumber like :orderNum ");

			if(!"".equals(facCode) && null != facCode){
				sb.append(" AND order.name = :facCode ");
				flag_check_name=true;
			}
			if(!"ALL".equals(status) && null != status){
				sb.append(" AND order.status = :status ");
				flag_check_status=true;
			}
			if(!"".equals(amountSt) && null != amountSt){
				sb.append(" AND order.total >= :amountStCov ");
				flag_check_amount_st=true;
			}
			if(!"".equals(amountEd) && null != amountEd){
				sb.append(" AND order.total <= :amountEdCov ");
				flag_check_amount_ed=true;
			}
			if((!"".equals(dateSt) && null != dateSt)){
				sb.append(" AND order.submitDate >=  :stDateConv ");
				flag_check_date_st=true;
			}
			if((!"".equals(dateEd) && null != dateEd)){
				sb.append(" AND order.submitDate <=  :edDateConv ");
				flag_check_date_ed=true;
			}

			sb.append(" ORDER BY  order.submitDate ");
			LOG.info("query : "+sb.toString());
			final Query query = em.createQuery(sb.toString());

			if(flag_check_name){
				query.setParameter("facCode", facCode);
			}

			if(flag_check_status){
				query.setParameter("status", status);
			}

			if(flag_check_amount_st){
				BigDecimal amountStCov = new BigDecimal(amountSt);
				query.setParameter("amountStCov", amountStCov);
			}
			if(flag_check_amount_ed){
				BigDecimal amountEdCov = new BigDecimal(amountEd);
				query.setParameter("amountEdCov", amountEdCov);
			}
			if(flag_check_date_st){
				dateSt = dateSt.replaceAll("/","-");
				Timestamp ts = DateUtil.getTimeStamp(dateSt);
				query.setParameter("stDateConv", ts);
			}
			if(flag_check_date_ed){
				dateEd = dateSt.replaceAll("/","-");
				Timestamp ts = DateUtil.getTimeStampGetMaxTime(dateEd);
				query.setParameter("edDateConv", ts);
			}

			query.setParameter("customer", "%"+customer+"%");
			query.setParameter("orderNum", "%"+orderNum+"%");
			resultOrder = query.getResultList();

			return resultOrder;
		}catch (Exception e){
			LOG.error("findOrderbyCriteria Exception Msg : "+e.getMessage());
			e.printStackTrace();
			return	resultOrder;
		}
	}

	@Override
	public Order findOrderByOrderNumber(String orderNumber) {
	    List<Order> resultOrder = new ArrayList<>();
        StringBuilder sb = new StringBuilder();
        sb.append(" SELECT order ");
        sb.append(" FROM org.broadleafcommerce.core.order.domain.Order order ");
        sb.append(" WHERE order.orderNumber = :orderNum ");

        final Query query = em.createQuery(sb.toString());
        query.setParameter("orderNum", orderNumber);

        resultOrder = query.getResultList();
        if(!resultOrder.isEmpty()){
            return resultOrder.get(0);
        }

		return null;
	}


}
