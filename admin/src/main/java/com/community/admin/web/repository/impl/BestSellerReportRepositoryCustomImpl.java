package com.community.admin.web.repository.impl;

import com.community.admin.web.repository.BestSellerReportRepositoryCustom;
import com.community.admin.web.util.ValidateUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository("blBestSellerReportCustomRepo")
public class BestSellerReportRepositoryCustomImpl implements BestSellerReportRepositoryCustom {

	private static final Log LOG = LogFactory.getLog(BestSellerReportRepositoryCustomImpl.class);

	@PersistenceContext(unitName = "blPU", type = PersistenceContextType.TRANSACTION)
	protected EntityManager em;


	@Override
	public List<Map<String, Object>> queryReportByMonth_RoleAdmin(String manFac, String facCode, String year, String month, String limitMax) {

		LOG.info("===- queryReportByMonth_RoleAdmin -===");

		List<Object[]> listfromQuery = new ArrayList<Object[]>();
		List<Map<String, Object>> result = new ArrayList<>();

		boolean flag_check_manufac = false;
		boolean flag_check_limit = false;
		boolean flag_check_facCode = false;

		StringBuilder sqlquery = new StringBuilder();
		sqlquery.append(" SELECT od.name as oName, sku.name as sName, p.manufacture, sum(odi.quantity), to_char(od.submit_date,'YYYY-MM') as oDate ");
		sqlquery.append(" FROM blc_order od ");
		sqlquery.append(" RIGHT JOIN blc_order_item as odi on od.order_id = odi.order_id ");
		sqlquery.append(" RIGHT JOIN blc_sku as sku on sku.name = odi.name");
		sqlquery.append(" RIGHT JOIN blc_product p on p.product_id = sku.default_product_id ");
		sqlquery.append(" WHERE to_char(od.submit_date, 'YYYY') =  :year ");
		sqlquery.append(" AND to_char(od.submit_date, 'MM') = :month ");
		sqlquery.append(" AND odi.quantity is not null ");
		sqlquery.append(" AND od.order_status = 'APPROVE' ");

		if(!"".equals(facCode) && null != facCode){
			sqlquery.append(" AND od.name  = :facCode ");
			flag_check_facCode=true;
		}

		if(!"".equals(manFac) && null != manFac){
			sqlquery.append(" AND p.manufacture like :manFac ");
			flag_check_manufac=true;
		}

		sqlquery.append(" GROUP by oName,sName,p.manufacture, oDate ");
		sqlquery.append(" ORDER by sum(odi.quantity) desc ");
		if(!"ALL".equals(limitMax.toUpperCase())){
			sqlquery.append(" LIMIT :max ");
			flag_check_limit = true;
		}

		Query query = em.createNativeQuery(sqlquery.toString());
		if(flag_check_facCode){
			query.setParameter("facCode", facCode);
		}
		if(flag_check_manufac){
			query.setParameter("manFac", "%"+manFac+"%");
		}

		if(flag_check_limit){
			query.setParameter("max",Long.valueOf(limitMax));
		}
		if(month.length() == 1){
			month = "0".concat(month);

		}
		query.setParameter("year", year);
		query.setParameter("month",month);
		listfromQuery = query.getResultList();
		if(ValidateUtil.isNotEmpty(listfromQuery)){
			for(Object[] o : listfromQuery){
				Map<String, Object> mapResult = new HashMap();
				mapResult.put("factoryCode", o[0]);
				mapResult.put("productName", o[1]);
				mapResult.put("manufacture", o[2]);
				mapResult.put("itemAmount", o[3]);
				mapResult.put("year", String.valueOf(o[4]).split("-")[0]);
				mapResult.put("month", String.valueOf(o[4]).split("-")[1]);

				result.add(mapResult);
			}
		}
		return result;
	}

	@Override
	public List<Map<String, Object>> queryReportByYear_RoleAdmin(String manFac, String facCode, String year, String limitMax) {
		LOG.info("===- queryReportByYear_RoleAdmin -===");

		List<Object[]> listfromQuery = new ArrayList<Object[]>();
		List<Map<String, Object>> result = new ArrayList<>();

		boolean flag_check_manufac = false;
		boolean flag_check_facCode = false;
		boolean flag_check_limit = false;

		StringBuilder sqlquery = new StringBuilder();
		sqlquery.append(" SELECT od.name as oName, sku.name as sName, p.manufacture, sum(odi.quantity), to_char(od.submit_date,'YYYY') as oDate ");
		sqlquery.append(" FROM blc_order od ");
		sqlquery.append(" RIGHT JOIN blc_order_item as odi on od.order_id = odi.order_id ");
		sqlquery.append(" RIGHT JOIN blc_sku as sku on sku.name = odi.name");
		sqlquery.append(" RIGHT JOIN blc_product p on p.product_id = sku.default_product_id ");
		sqlquery.append(" WHERE to_char(od.submit_date, 'YYYY') =  :year ");
		sqlquery.append(" AND odi.quantity is not null ");
		sqlquery.append(" AND od.order_status = 'APPROVE' ");

		if(!"".equals(facCode) && null != facCode){
			sqlquery.append(" AND od.name  = :facCode ");
			flag_check_facCode=true;
		}

		if(!"".equals(manFac) && null != manFac){
			sqlquery.append(" AND p.manufacture like :manFac ");
			flag_check_manufac=true;
		}

		sqlquery.append(" GROUP by oName,sName,p.manufacture, oDate ");
		sqlquery.append(" ORDER by sum(odi.quantity) desc ");
		if(!"ALL".equals(limitMax.toUpperCase())){
			sqlquery.append(" LIMIT :max ");
			flag_check_limit = true;
		}

		Query query = em.createNativeQuery(sqlquery.toString());
		if(flag_check_facCode){
			query.setParameter("facCode", facCode);
		}
		if(flag_check_manufac){
			query.setParameter("manFac", "%"+manFac+"%");
		}

		if(flag_check_limit){
			query.setParameter("max",Long.valueOf(limitMax));
		}
		query.setParameter("year", year);
		listfromQuery = query.getResultList();
		if(ValidateUtil.isNotEmpty(listfromQuery)){
			for(Object[] o : listfromQuery){
				Map<String, Object> mapResult = new HashMap();
				mapResult.put("factoryCode", o[0]);
				mapResult.put("productName", o[1]);
				mapResult.put("manufacture", o[2]);
				mapResult.put("itemAmount", o[3]);
				mapResult.put("year", o[4]);

				result.add(mapResult);
			}
		}
		return result;
	}

	@Override
	public List<Map<String, Object>> queryReportByMonth_RoleVender(String manFac, String year, String month, String limitMax) {

		LOG.info("===- queryReportByMonth_RoleVender -===");

		List<Object[]> listfromQuery = new ArrayList<Object[]>();
		List<Map<String, Object>> result = new ArrayList<>();

		boolean flag_check_manufac = false;
		boolean flag_check_limit = false;

		StringBuilder sqlquery = new StringBuilder();
		sqlquery.append(" SELECT od.name as oName, sku.name as sName, p.manufacture, sum(odi.quantity), to_char(od.submit_date,'YYYY-MM') as oDate ");
		sqlquery.append(" FROM blc_order od ");
		sqlquery.append(" LEFT JOIN blc_order_item as odi on od.order_id = odi.order_id ");
		sqlquery.append(" LEFT JOIN blc_sku as sku on sku.name = odi.name");
		sqlquery.append(" LEFT JOIN blc_product p on p.product_id = sku.default_product_id ");
		sqlquery.append(" WHERE od.name is not null ");
		sqlquery.append(" AND odi.quantity is not null ");
		sqlquery.append(" AND to_char(od.submit_date, 'MM') = :month ");
		sqlquery.append(" AND p.manufacture is not null ");

		if(!"".equals(manFac) && null != manFac){
			sqlquery.append(" AND p.manufacture like :manFac ");
			flag_check_manufac=true;
		}

		sqlquery.append(" AND to_char(od.submit_date, 'YYYY') =  :year ");
		sqlquery.append(" GROUP by oName,sName,p.manufacture, oDate ");
		sqlquery.append(" ORDER by sum(odi.quantity) desc ");
		if(!"ALL".equals(limitMax.toUpperCase())){
			sqlquery.append(" LIMIT :max ");
			flag_check_limit = true;
		}

		Query query = em.createNativeQuery(sqlquery.toString());

		if(flag_check_manufac){
			query.setParameter("manFac", "%"+manFac+"%");
		}

		query.setParameter("year", year);
		if(flag_check_limit){
			query.setParameter("max",Long.valueOf(limitMax));
		}
		if(month.length() == 1){
			month = "0".concat(month);

		}
		query.setParameter("month",month);
		listfromQuery = query.getResultList();
		if(ValidateUtil.isNotEmpty(listfromQuery)){
			for(Object[] o : listfromQuery){
				Map<String, Object> mapResult = new HashMap();
				mapResult.put("factoryCode", o[0]);
				mapResult.put("productName", o[1]);
				mapResult.put("manufacture", o[2]);
				mapResult.put("itemAmount", o[3]);
				mapResult.put("year", String.valueOf(o[4]).split("-")[0]);
				mapResult.put("month", String.valueOf(o[4]).split("-")[1]);

				result.add(mapResult);
			}
		}
		return result;
	}

	@Override
	public List<Map<String, Object>> queryReportByYear_RoleVender(String manFac, String year, String limitMax) {
		LOG.info("===- queryReportByYear_RoleVender -===");

		List<Object[]> listfromQuery = new ArrayList<Object[]>();
		List<Map<String, Object>> result = new ArrayList<>();

		boolean flag_check_manufac = false;
		boolean flag_check_year = false;
		boolean flag_check_limit = false;

		StringBuilder sqlquery = new StringBuilder();
		sqlquery.append(" SELECT od.name as oName, sku.name as sName, p.manufacture, sum(odi.quantity), to_char(od.submit_date,'YYYY') as oDate ");
		sqlquery.append(" FROM blc_order od ");
		sqlquery.append(" LEFT JOIN blc_order_item as odi on od.order_id = odi.order_id ");
		sqlquery.append(" LEFT JOIN blc_sku as sku on sku.name = odi.name");
		sqlquery.append(" LEFT JOIN blc_product p on p.product_id = sku.default_product_id ");
		sqlquery.append(" WHERE od.name is not null ");
		sqlquery.append(" AND odi.quantity is not null ");
		sqlquery.append(" AND p.manufacture is not null ");

		if(!"".equals(manFac) && null != manFac){
			sqlquery.append(" AND p.manufacture like :manFac ");
			flag_check_manufac=true;
		}

		if((!"".equals(year) && null != year)){
			sqlquery.append(" AND to_char(od.submit_date, 'YYYY') =  :year ");
			flag_check_year=true;
		}

		sqlquery.append(" GROUP by oName,sName,p.manufacture, oDate ");
		sqlquery.append(" ORDER by sum(odi.quantity) desc ");
		if(!"ALL".equals(limitMax.toUpperCase())){
			sqlquery.append(" LIMIT :max ");
			flag_check_limit = true;
		}

		Query query = em.createNativeQuery(sqlquery.toString());

		if(flag_check_manufac){
			query.setParameter("manFac", "%"+manFac+"%");
		}
		if(flag_check_year){
			query.setParameter("year", year);
		}
		if(flag_check_limit){
			query.setParameter("max",Long.valueOf(limitMax));
		}
		listfromQuery = query.getResultList();
		if(ValidateUtil.isNotEmpty(listfromQuery)){
			for(Object[] o : listfromQuery){
				Map<String, Object> mapResult = new HashMap();
				mapResult.put("factoryCode", o[0]);
				mapResult.put("productName", o[1]);
				mapResult.put("manufacture", o[2]);
				mapResult.put("itemAmount", o[3]);
				mapResult.put("year", o[4]);

				result.add(mapResult);
			}
		}
		return result;
	}
}
