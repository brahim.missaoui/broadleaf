package com.community.admin.web.service.impl;

import com.community.admin.web.repository.BestSellerReportRepositoryCustom;
import com.community.admin.web.service.BestSellerReportService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jmx.export.annotation.ManagedResource;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Service("blBestSellerReportCustomService")
@ManagedResource(objectName="org.broadleafcommerce:name=OrderCustomService", description="Order Custom Service", currencyTimeLimit=15)
public class BestSellerReportServiceImpl implements BestSellerReportService {

	private static final Log LOG = LogFactory.getLog(BestSellerReportServiceImpl.class);

	@Resource(name = "blBestSellerReportCustomRepo")
	protected BestSellerReportRepositoryCustom bestSellerReportRepositoryCustom;

	@Override
	public List<Map<String, Object>> queryReportByMonth_RoleAdmin(String manFac, String facCode, String year, String month, String limitMax) {
		return bestSellerReportRepositoryCustom.queryReportByMonth_RoleAdmin(manFac,facCode,year,month,limitMax);
	}

	@Override
	public List<Map<String, Object>> queryReportByYear_RoleAdmin(String manFac, String facCode, String year, String limitMax) {
		return bestSellerReportRepositoryCustom.queryReportByYear_RoleAdmin(manFac,facCode,year,limitMax);
	}

	@Override
	public List<Map<String, Object>> queryReportByMonth_RoleVender(String manFac, String year, String month, String limitMax) {
		return bestSellerReportRepositoryCustom.queryReportByMonth_RoleVender(manFac,year,month,limitMax);
	}

	@Override
	public List<Map<String, Object>> queryReportByYear_RoleVender(String manFac, String year, String limitMax) {
		return bestSellerReportRepositoryCustom.queryReportByYear_RoleVender(manFac,year,limitMax);
	}
}
