package com.community.admin.configuration.aop;


import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.broadleafcommerce.openadmin.server.security.dao.AdminUserDao;
import org.broadleafcommerce.openadmin.server.security.domain.AdminUser;
import org.broadleafcommerce.openadmin.server.security.domain.AdminUserAttributeImpl;
import org.springframework.stereotype.Component;

import com.community.admin.web.service.UserProfileService;
import com.protosstechnology.commons.util.BeansUtil;


@Component
@Aspect
public class UpdateUserFactoryAspect {
    private static final Log LOG = LogFactory.getLog(UpdateUserFactoryAspect.class);
    
    @Resource(name = "blAdminUserDao")
    protected AdminUserDao adminUserDao;
    
    @PersistenceContext(unitName = "blPU")
    protected EntityManager em;
    
    @Resource(name = "userProfileService")
    UserProfileService userProfileService;


    @Pointcut("execution(*   org.broadleafcommerce.openadmin.server.security.dao.AdminUserDaoImpl.saveAdminUser*(..))")
    public void saveAdminUser() {
    }

    @Before("saveAdminUser()")
    public void saveAdminUser(JoinPoint jp) throws Throwable {
        String methodName = jp.getSignature().getName();
        AdminUser adminUser = (AdminUser)jp.getArgs()[0];
        if(adminUser.getId() != null 
        		&& adminUser.getAdditionalFields().get("FACTORY_CODE") == null ) {
        	
        	String factoryCode = userProfileService.getFactoryCode(adminUser.getLogin());
        	if(BeansUtil.isNotEmpty(factoryCode)) {
        		AdminUserAttributeImpl userAttribute = new AdminUserAttributeImpl();
            	userAttribute.setAdminUser(adminUser);
            	userAttribute.setName("FACTORY_CODE");
            	userAttribute.setValue(factoryCode);
            	em.persist(userAttribute);
            	LOG.info("===================persist "+adminUser.getLogin()+" FACTORY_CODE="+factoryCode);
        	}
        	
        	
        }
        
        
        
        
    }
    

}

