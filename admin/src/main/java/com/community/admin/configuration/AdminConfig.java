package com.community.admin.configuration;

import org.apache.catalina.connector.Connector;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.broadleafcommerce.common.extensibility.context.merge.Merge;
import org.broadleafcommerce.core.search.service.solr.index.SolrIndexService;
import org.quartz.JobDetail;
import org.quartz.Trigger;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.context.annotation.*;

import com.community.core.config.CoreConfig;
import com.community.core.config.StringFactoryBean;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.quartz.MethodInvokingJobDetailFactoryBean;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.scheduling.quartz.SimpleTriggerFactoryBean;

import java.util.Arrays;
import java.util.List;
//import org.broadleafcommerce.cms.structure.service.StructuredContentService;
/**
 * @author Elbert Bautista (elbertbautista)
 */
@Configuration
@EnableScheduling
@Import({
        CoreConfig.class,
        AdminSecurityConfig.class
})
@ImportResource({
        "classpath:applicationContext-admin.xml",
})
public class AdminConfig {
    private static final Log LOG = LogFactory.getLog(AdminConfig.class);


    @Bean
    @ConditionalOnProperty("jmx.app.name")
    public StringFactoryBean blJmxNamingBean() {
        return new StringFactoryBean();
    }

    @Merge("blMergedCacheConfigLocations")
    public List<String> adminOverrideCache() {
        return Arrays.asList("classpath:bl-override-ehcache-admin.xml");
    }

    @Bean
    public TomcatServletWebServerFactory tomcatEmbeddedServletContainerFactory(@Value("${http.server.port:8081}") int httpServerPort) {
        TomcatServletWebServerFactory tomcat = new TomcatServletWebServerFactory();
        tomcat.addAdditionalTomcatConnectors(createStandardConnector(httpServerPort));
        return tomcat;
    }

    private Connector createStandardConnector(int port) {
        Connector connector = new Connector("org.apache.coyote.http11.Http11NioProtocol");
        connector.setPort(port);
        return connector;
    }


    @Configuration
    public static class SolrReindexConfig {

        @Bean
        public SchedulerFactoryBean rebuildIndexScheduler(@Qualifier("rebuildIndexTrigger") Trigger rebuildIndexTrigger) {
            SchedulerFactoryBean scheduler = new SchedulerFactoryBean();
            scheduler.setTriggers(rebuildIndexTrigger);
            return scheduler;
        }



        @Bean
        public SimpleTriggerFactoryBean rebuildIndexTrigger(@Qualifier("solrReindexJobDetail") JobDetail detail,
                                                            @Value("${solr.index.start.delay}") long startDelay,
                                                            @Value("${solr.index.repeat.interval}") long repeatInterval) {

            SimpleTriggerFactoryBean trigger = new SimpleTriggerFactoryBean();
            trigger.setJobDetail(detail);
            trigger.setStartDelay(startDelay);
            trigger.setRepeatInterval(repeatInterval);
            return trigger;
        }

        @Bean
        public FactoryBean<JobDetail> solrReindexJobDetail(SolrIndexService indexService) {

            MethodInvokingJobDetailFactoryBean detail = new MethodInvokingJobDetailFactoryBean();
            detail.setTargetObject(indexService);
            detail.setTargetMethod("rebuildIndex");

            return detail;
        }
    }




}

