package com.community.admin.configuration.aop;


import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.broadleafcommerce.openadmin.server.security.dao.AdminUserDao;
import org.broadleafcommerce.openadmin.web.form.entity.EntityForm;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;


@Component
@Aspect
public class VendorProductAspect {
    private static final Log LOG = LogFactory.getLog(VendorProductAspect.class);
    
    @Resource(name = "blAdminUserDao")
    protected AdminUserDao adminUserDao;


    @Pointcut("execution(*   org.broadleafcommerce.openadmin.web.controller.entity.AdminBasicEntityController.*(..))")
    public void redirectVendor() {
    }

    @Before("redirectVendor()")
    public void redirectVendor(JoinPoint jp) throws Throwable {
        String methodName = jp.getSignature().getName();

        //LOG.info("===================VendorProductAspect ="+methodName);
        
        
        
        if(jp.getArgs().length > 1 &&
        		jp.getArgs()[0] instanceof HttpServletRequest &&
        		jp.getArgs()[1] instanceof HttpServletResponse ) {
        	
        	HttpServletRequest httpServletRequest   = (HttpServletRequest) jp.getArgs()[0];
        	HttpServletResponse httpServletResponse = (HttpServletResponse) jp.getArgs()[1];
        	

        	Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        	Collection<GrantedAuthority> authorities = (Collection<GrantedAuthority>) authentication.getAuthorities();
        	List<GrantedAuthority> listAuthorities = new ArrayList<GrantedAuthority>();
        	listAuthorities.addAll(authorities);
        	
        	boolean isVendor = false;
        	for(GrantedAuthority authority :listAuthorities) {
        		if("ROLE_VENDOR".equals(authority.getAuthority())) {
        			isVendor = true;
        			break;
        		}
        	}
        	String queryString = "";
        	if(httpServletRequest.getQueryString()!=null) {
        		queryString = "?"+httpServletRequest.getQueryString();
        	}
        	String newUrl = httpServletRequest.getContextPath()+"/vendor"+httpServletRequest.getServletPath()+queryString;
        	
        	
        	if(isVendor 
        			&& httpServletRequest.getServletPath().startsWith("/product")
        			&& !httpServletRequest.getServletPath().startsWith("/vendor") ) {
        		httpServletResponse.sendRedirect(newUrl);
        	}
        	
        	if(isVendor && "viewEntityList".equals(methodName)) {
            	if(jp.getArgs().length > 4 &&
                		jp.getArgs()[4] instanceof MultiValueMap ) {
            		MultiValueMap<String, String> requestParams = (MultiValueMap<String, String>) jp.getArgs()[4];
            		String userName = SecurityContextHolder.getContext().getAuthentication().getName();
            		String name = adminUserDao.readAdminUserByUserName(userName).getName();
            		requestParams.add("manufacturer", name);
            	}
            }
        	
        }
    }
    
    @Around("execution(*   org.broadleafcommerce.openadmin.web.controller.entity.AdminBasicEntityController.saveEntityJson(..))")
	public Object employeeAroundAdvice(ProceedingJoinPoint proceedingJoinPoint){
		Object[] args = proceedingJoinPoint.getArgs();
		EntityForm entityForm = (EntityForm) args[5];
		
		String userName = SecurityContextHolder.getContext().getAuthentication().getName();
		String name = adminUserDao.readAdminUserByUserName(userName).getName();
		
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    	Collection<GrantedAuthority> authorities = (Collection<GrantedAuthority>) authentication.getAuthorities();
    	List<GrantedAuthority> listAuthorities = new ArrayList<GrantedAuthority>();
    	listAuthorities.addAll(authorities);
    	
    	boolean isVendor = false;
    	for(GrantedAuthority authority :listAuthorities) {
    		if("ROLE_VENDOR".equals(authority.getAuthority())) {
    			isVendor = true;
    			break;
    		}
    	}
		
		if(isVendor && "org.broadleafcommerce.cms.file.domain.ImageStaticAssetImpl".equals(entityForm.getEntityType())) {
			entityForm.findField("title").setValue(userName);
		}else if(isVendor &&  "org.broadleafcommerce.core.catalog.domain.ProductImpl".equals(entityForm.getEntityType())) {
			entityForm.findField("manufacturer").setValue(name);
			entityForm.findField("defaultSku.activeStartDate").setValue("2050.12.31 00:00:00");
			entityForm.findField("defaultSku.inventoryType").setValue("WAIT_APPROVE");
		}
		
		Object value = null;
		try {
			value = proceedingJoinPoint.proceed(args);
		} catch (Throwable e) { e.printStackTrace(); }
		return value;
	}
    

    @Around("execution(*   org.broadleafcommerce.openadmin.web.controller.entity.AdminBasicEntityController.addEntity(..))")
	public Object addEntity(ProceedingJoinPoint proceedingJoinPoint){
		Object[] args = proceedingJoinPoint.getArgs();
		EntityForm entityForm = (EntityForm) args[4];
		
		String userName = SecurityContextHolder.getContext().getAuthentication().getName();
		String name = adminUserDao.readAdminUserByUserName(userName).getName();
		
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    	Collection<GrantedAuthority> authorities = (Collection<GrantedAuthority>) authentication.getAuthorities();
    	List<GrantedAuthority> listAuthorities = new ArrayList<GrantedAuthority>();
    	listAuthorities.addAll(authorities);
    	
    	boolean isVendor = false;
    	for(GrantedAuthority authority :listAuthorities) {
    		if("ROLE_VENDOR".equals(authority.getAuthority())) {
    			isVendor = true;
    			break;
    		}
    	}
    	
		if(isVendor &&  "org.broadleafcommerce.cms.file.domain.ImageStaticAssetImpl".equals(entityForm.getEntityType())) {
			entityForm.findField("title").setValue(userName);
		}else if(isVendor &&  "org.broadleafcommerce.core.catalog.domain.ProductImpl".equals(entityForm.getEntityType())) {
			entityForm.findField("manufacturer").setValue(name);
			entityForm.findField("defaultSku.activeStartDate").setValue("2050.12.31 00:00:00");
			entityForm.findField("defaultSku.inventoryType").setValue("WAIT_APPROVE");
		}
		
		
		Object value = null;
		try {
			value = proceedingJoinPoint.proceed(args);
		} catch (Throwable e) { e.printStackTrace(); }
		return value;
	}

}

