package com.community.repository;

import com.community.model.ResponseModel;

public interface SystemPropertiesRepositoryCustom {
    ResponseModel findByFriendlyGroup(String friendlyGroup);
}
