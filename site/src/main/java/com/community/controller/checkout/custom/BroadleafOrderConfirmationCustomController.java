package com.community.controller.checkout.custom;

import org.broadleafcommerce.common.web.controller.BroadleafAbstractController;
import org.broadleafcommerce.core.order.domain.Order;
import org.broadleafcommerce.core.order.service.OrderService;
import org.broadleafcommerce.core.web.controller.checkout.ConfirmationControllerExtensionManager;
import org.broadleafcommerce.profile.core.domain.Customer;
import org.broadleafcommerce.profile.web.core.CustomerState;
import org.springframework.ui.Model;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class BroadleafOrderConfirmationCustomController extends BroadleafAbstractController {

    @Resource(name = "blOrderService")
    protected OrderService orderService;
    
    @Resource(name = "blConfirmationControllerExtensionManager")
    protected ConfirmationControllerExtensionManager extensionManager;
    
    protected static String orderConfirmationView = "checkout/confirmation";

    public String displayOrderConfirmationByOrderNumber(String orderNumber, Model model,
             HttpServletRequest request, HttpServletResponse response) {
    	Order order = orderService.findOrderByOrderNumber(orderNumber);
        if (order != null ) {
            extensionManager.getProxy().processAdditionalConfirmationActions(order);

            model.addAttribute("order", order);
            return getOrderConfirmationView();
        }
        
        return "redirect:/";
    }

    public String displayOrderConfirmationByOrderId(Long orderId, Model model,
             HttpServletRequest request, HttpServletResponse response) {

        Customer customer = CustomerState.getCustomer();
        if (customer != null) {
            Order order = orderService.findOrderById(orderId);
            if (order != null && customer.equals(order.getCustomer())) {
                extensionManager.getProxy().processAdditionalConfirmationActions(order);

                model.addAttribute("order", order);
                return getOrderConfirmationView();
            }
        }
        return "redirect:/";
    }

    public String getOrderConfirmationView() {
        return orderConfirmationView;
    }
}