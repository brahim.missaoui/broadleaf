package com.community.controller.account;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class AuthenticationController {

	@Autowired
	private AuthenticationManager  authenticationManager;
	 
	 @RequestMapping(value = "/authenpass", method = RequestMethod.GET)
	 public void  goTohome(@RequestParam String username,@RequestParam String password,@RequestParam(required=false) String appToken,HttpServletRequest request,HttpServletResponse response) throws IOException {
			  try{
				  authenticateUser(username,password,request);
			  }
			  catch(Exception e){
				  e.printStackTrace();
			  }
			  
			  response.sendRedirect(request.getContextPath());
	}
	 
	private void authenticateUser(String username,String password, HttpServletRequest request) {
	        
	        UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(username, password);
	        request.getSession();
	        authToken.setDetails(new WebAuthenticationDetails(request));
	        Authentication authentication= authenticationManager.authenticate(authToken);
	        SecurityContextHolder.getContext(). 
	        setAuthentication(authentication);
	    
	}

}
