package com.community.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.broadleafcommerce.core.order.domain.Order;
import org.broadleafcommerce.core.order.service.type.OrderStatus;
import org.springframework.stereotype.Repository;

import com.community.dao.OrderCustomDao;

@Repository("blOrderCustomDao")
public class OrderCustomDaoImpl implements OrderCustomDao{
	
	private static final Log LOG = LogFactory.getLog(OrderCustomDaoImpl.class);
	
	@PersistenceContext(unitName = "blPU")
	protected EntityManager em;

	@Override
	public List<Order> readOrdersForCustomerName(String name, OrderStatus orderStatus) {
		LOG.debug("======================== OrderCustomDaoImpl readOrdersForCustomerName orderStatus="+orderStatus);
		List<Order> resultOrder = null;
		if (orderStatus != null) {
			StringBuilder sb = new StringBuilder();
			sb.append(" SELECT order ");
			sb.append(" FROM org.broadleafcommerce.core.order.domain.Order order ");
			sb.append(" WHERE order.name   = :customerName ");
			sb.append(" AND   order.status = :orderStatus ");
			sb.append(" ORDER BY  order.submitDate ");
			final Query query = em.createQuery(sb.toString());
            query.setParameter("customerName", name);
            query.setParameter("orderStatus", orderStatus.getType());
            resultOrder = query.getResultList();
		}
		return resultOrder;
	}


}
