package com.community.service.impl;

import com.community.model.ResponseModel;
import com.community.repository.SystemPropertiesRepositoryCustom;
import com.community.service.SystemPropertiesCustomService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jmx.export.annotation.ManagedResource;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service("blSystemPropertiesCustomService")
@ManagedResource(objectName="org.broadleafcommerce:name=OrderCustomService", description="SystemProperties Custom Service", currencyTimeLimit=15)
public class SystemPropertiesCustomServiceImpl implements SystemPropertiesCustomService {
	
	private static final Log LOG = LogFactory.getLog(SystemPropertiesCustomServiceImpl.class);
	
	@Resource(name = "blSystemPropertiesCustomRepo")
    protected SystemPropertiesRepositoryCustom systemPropertiesRepositoryCustom;

	@Override
	public ResponseModel findByFriendlyGroup(String friendlyGroup) {
		return systemPropertiesRepositoryCustom.findByFriendlyGroup(friendlyGroup);
	}
}
