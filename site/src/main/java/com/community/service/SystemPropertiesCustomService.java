package com.community.service;

import com.community.model.ResponseModel;

public interface SystemPropertiesCustomService {

	ResponseModel findByFriendlyGroup(String friendlyGroup);
}
