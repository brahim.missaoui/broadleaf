package com.community.service;

import org.springframework.http.ResponseEntity;

public interface FlowProcessService {

	public ResponseEntity<String> createFlow(String orderNumber);
}
