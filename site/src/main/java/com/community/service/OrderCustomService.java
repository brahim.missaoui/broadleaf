package com.community.service;

import java.util.List;

import org.broadleafcommerce.core.order.domain.Order;
import org.broadleafcommerce.core.order.service.type.OrderStatus;

public interface OrderCustomService {

	List<Order> findOrdersForFactoryCode(String factoryCode, OrderStatus status);
}
