let listOffBanner = []
let listDelayImage = []
let haveBanner = 0
$(document ).ready(function() {
    loadEnableImage()
    loadSrcImg()
    loadDelayImage()
    setTimeout(function () {
        if(haveBanner != 0){
            loopSlide();
            $('#myCarousel').removeClass('hide')
        }
    },1000)


    $('#playButton').click(function () {
        $('#myCarousel').carousel('cycle');
    });
    $('#pauseButton').click(function () {
        $('#myCarousel').carousel('pause');
    });

});

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}


async function loopSlide(){
    for(let i=0; i<listDelayImage.length; i++){
        console.log('sleep = '+listDelayImage[i]+'s')
        await sleep(1000*listDelayImage[i]);
        $('#myCarousel').carousel('next');
        if(i==listDelayImage.length-1)
            i=-1;
    }
}



function loadSrcImg(){
	var contextRoot = $('#contextRoot').val();
	
    let keySrcImg = 'banner.src'
    $.ajax({
        type: "GET",
        url: window.location.origin+contextRoot+'requestSystemProperties/findByFriendlyGroup' ,
        async:true,
        data : {
            friendlyGroup : keySrcImg
        },
        complete: function (xhr) {
            if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                    if(xhr.responseText){
                        let obj = JSON.parse(xhr.responseText);
                        let listSrcImg = obj.data
                        if(listSrcImg != null && listSrcImg != undefined){
                            $('#section_banner').empty()
                            let count = 0;
                            for(let i=0; i<listSrcImg.length; i++){

                                if(listOffBanner[i] != 0){

                                    $('#section_banner').append('' +
                                        '<div id="banner_'+listSrcImg[i].property_name+'" class="item '+( count ==0 ? 'active' : '')+' ">' +
                                        '<img class="img-responsive full-width" src="'+listSrcImg[i].property_value+'" />' +
                                        '</div>' +
                                        '')
                                    count++;
                                }

                            }
                        }

                    }else{
                        console.log('NOT FOUND')
                    }

                }
            }
        }
    }).done(function (){
        //close loader
    });

}
function loadEnableImage(){
	var contextRoot = $('#contextRoot').val();
	
    let keySrcImg = 'banner.enable'
    $.ajax({
        type: "GET",
        url: window.location.origin+contextRoot+'requestSystemProperties/findByFriendlyGroup' ,
        async:true,
        data : {
            friendlyGroup : keySrcImg
        },
        complete: function (xhr) {
            if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                    if(xhr.responseText){
                        let obj = JSON.parse(xhr.responseText);
                        let listEnableImg = obj.data

                        if(listEnableImg != null && listEnableImg != undefined){
                            listOffBanner = []
                            for(let i=0; i<listEnableImg.length; i++){
                                listOffBanner.push(parseInt(listEnableImg[i].property_value))
                                console.log(parseInt(listEnableImg[i].property_value) == 1)
                                if(parseInt(listEnableImg[i].property_value) == 1){
                                    haveBanner++
                                }
                            }
                        }
                    }else{
                        console.log('NOT FOUND')
                    }

                }
            }
        }
    }).done(function (){
        //close loader
    });
}
function loadDelayImage(){
	var contextRoot = $('#contextRoot').val();
	
    let keySrcImg = 'banner.delay'
    $.ajax({
        type: "GET",
        url: window.location.origin+contextRoot+'requestSystemProperties/findByFriendlyGroup' ,
        async:true,
        data : {
            friendlyGroup : keySrcImg
        },
        complete: function (xhr) {
            if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                    if(xhr.responseText){
                        let obj = JSON.parse(xhr.responseText);
                        let listDelayImg = obj.data

                        if(listDelayImg != null && listDelayImg != undefined){
                            listDelayImage = []
                            for(let i=0; i<listDelayImg.length; i++){
                                if(listOffBanner[i] != 0){
                                    listDelayImage.push(parseInt(listDelayImg[i].property_value))
                                }
                            }
                        }
                    }else{
                        console.log('NOT FOUND')
                    }

                }
            }
        }
    }).done(function (){
        //close loader
    });
}