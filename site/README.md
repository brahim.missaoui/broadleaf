# Build Site
docker build -t mpg-mmarketplace-site .

# Run Site
docker run -v /Users/dev2/Desktop/BL/AssetFile:/usr/local/mmarket/AssetFile -v /Users/dev2/Desktop/BL/TmpAssetFile:/usr/local/mmarket/TmpAssetFile -p 8082:8082 -p 8443:8443  -it mpg-mmarketplace-site